from .action import Action3
from mud.events import AnswerEvent

class AnswerAction(Action3):
    EVENT = AnswerEvent
    ACTION = "type"
    RESOLVE_OBJECT = "resolve_for_operate"

    def __init__(self, subject, object):
        super().__init__(subject, "zombie", object)

    def resolve_object2(self):
        return self.object2
