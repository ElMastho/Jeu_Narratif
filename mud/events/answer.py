from .event import Event3

class AnswerEvent(Event3):
    NAME = "type"

    def perform(self):
        self.add_prop("answerd-"+self.object2)
        self.inform("type")
