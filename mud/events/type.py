from .event import Event3

class TypeEvent(Event3):
    NAME = "type"

    def perform(self):
        self.add_prop("typed-"+self.object2)
        self.inform("type")
